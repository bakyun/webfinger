package webfinger

import (
	"net/http"
)

// Service is the webfinger service containing the required
// HTTP handlers and defaults for webfinger implementations.
type Service struct {
	// PreHandlers are invoked at the start of each HTTP method, used to
	// setup things like caching, etc. You can delete or replace
	// a handler by setting service.PreHandlers[name] = nil
	PreHandlers map[string]http.Handler

	// Resolver is the interface for resolving user details
	Resolver Resolver

	// AllowHTTP allows working on HTTP requests
	AllowHTTP bool
}

// Default creates a new service with the default registered handlers
func Default(ur Resolver) *Service {
	s := &Service{}
	s.Resolver = ur

	s.PreHandlers = make(map[string]http.Handler)
	s.PreHandlers[NoCacheMiddleware] = http.HandlerFunc(noCache)
	s.PreHandlers[ContentTypeMiddleware] = http.HandlerFunc(jrdSetup)

	return s
}
