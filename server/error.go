package webfinger

import (
	"errors"
)

var (
	// ErrNotFound is given for a non-existant resource
	ErrNotFound = errors.New("resource not found")
)
