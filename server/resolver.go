package webfinger

import (
	"net/url"

	"gitlab.com/bakyun/jrd"
)

// The Resolver is how the webfinger service looks up a user or resource. The resolver
// must be provided by the developer using this package, as each webfinger
// service may be exposing a different set or users or resources or services.
type Resolver interface {
	// FindURI finds the resource for the given URI
	FindURI(uri *url.URL, rel []string) (*jrd.JRD, error)

	// FindUser finds the user given the username and hostname.
	FindUser(username, hostname string, rel []string) (*jrd.JRD, error)
}
